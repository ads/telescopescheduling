After submission (while creating the presentation), we became aware of a small typo in the report:

on page 11, in the Results subsection, for the discussion of test case 8, the lower bound and upper bound have been switched. It should have been (383.945 ; 384.311) instead of the current (384.311 ; 383.945).