import numpy as np
import offline as LP
from typing import Tuple, List
from functools import reduce

def ParseInput(src : str) -> Tuple[List[float], List[Tuple[float, float]]]:
    """This function parses input from a file for the telescope problem.
    The function will parse the first line to know how many pictures we want to send.
    The following n lines contain the sizes of the images.
    The next line contains the number of blackouts
    The next n lines contain the time and the length of the blackout seperated by a comma.

    Args:
        src (string): The name of the inputfile

    Returns:
        Tuple[List[float], List[Tuple[float, float]]: Returns 2 lists: one list with the images and one list with the blackouts
    """    
    contents = list()
    with open(src, 'r') as INPUT_FILE:
        contents = INPUT_FILE.readlines()

    imageCount = int(contents.pop(0))
    imageCollection = np.empty(imageCount)
    
    for i in range (0, imageCount):
        imageCollection[i] = float(contents.pop(0))
        
    blackoutCount = int(contents.pop(0))
    blackoutCollection = list() # tuples, so just a list, no need for numpy array
    
    for _ in range (0, blackoutCount):
        string = contents.pop(0).split(", ") # delimiter between start and end
        startTime = float(string[0])
        endTime = float(string[1])
        blackoutCollection.append((startTime, startTime + endTime))

    return (imageCollection, blackoutCollection)

def PrintOutput(filename : str, endTime : float, imageTimes : List[float]):
    """This function writes the output of the algorithm to a given outputfile
    The first lane contains the endTime of the last picture
    The other lines contain the startTime of each image

    Args:
        filename (str): The filename of the outputfile to write the output to
        endTime (float): The time between t0 and the end of the last picture
        imageTimes (List[float]): The startTimes for all the images
    """    
    with open(filename, 'w') as OUTPUT_FILE:
        OUTPUT_FILE.write(f"{str(endTime)}")
        OUTPUT_FILE.writelines(reduce(lambda x, y: x + "\n" + f"{str(y)}", imageTimes, ""))

    print(f"\nResults written to {filename}, program finished.")

if __name__ == "__main__":
    askinput = "Enter the test case number (0,1,2,3,4,5,6,7,8 or 9): "
    testcase = input(askinput)
    while testcase not in ["0","1","2","3","4","5","6","7","8","9"]:
        print("Incorrect, please only enter the number 0 through 9.")
        testcase = input(askinput)
    testcase = int(testcase)
    print(f"=====> START SOLVING TESTCASE {testcase}... <=====")
    parsedIn = ParseInput(f"t{testcase}_in.txt")
    res = LP.Solve(parsedIn[0], parsedIn[1])
    output_file = "t{}_out.txt".format(testcase)
    PrintOutput(output_file, res[0], res[1])