import gurobipy as gp
from gurobipy import GRB
import copy
from typing import List, Tuple

def Solve(images : List[int], blackouts : List[Tuple[int, int]]):
    """This function calculates the total processing time of the telescope problem given the lists of images and blackouts

    This function uses an ILP for solving this problem.

    Args:
        images (List[int]): A list with the sizes of the images that will be send between the blackouts
        blackouts (List[Tuple(int, int)]): A list with all the blackouts given their start and end time as a tuple

    Returns:
        Tuple[int, List[int]]: Returns a tuple with the total amount of time needed, and the start time for each image
    """    

    # if there are no blackouts, just send the images in after eachother, there is no unique optimal solution
    if not blackouts:
        finish_objective = 0
        starting_times = []
        for image in range(len(images)):
            starting_times.append(finish_objective)
            finish_objective += images[image]
        print("No blackboxes in the instance, easy solving without LP:")
        print(f"\nImage starting times: {starting_times}")
        print(f"=====> END TIME: {finish_objective} <=====")
        return (finish_objective, starting_times)

    whitebox_count = len(blackouts)+1 # extra whitebox until infinity after the last blackbox

    model = gp.Model("image_scheduling")

    """Refer to the paper for extensive elaboration

    SETS / INDICES"""
    f_i = images  # real number = image size

    # tuples (time_t, delta_t) = [start, duration)
    wb_t = calculate_whiteboxes(copy.deepcopy(blackouts))

    """"DECISION VARIABLES"""
    # objective function to minimize
    makespan = model.addVar(lb=0, vtype=GRB.CONTINUOUS)

    # Processing Volume = sum of length of images transmitted in whitebox t
    PV_t = model.addVars(whitebox_count, lb=0, vtype=GRB.CONTINUOUS)

    # image i in whitebox t: yes (1)/no (0)
    X_i_t = model.addVars(len(images), whitebox_count, vtype=GRB.BINARY)

    # whitebox t is empty (no images being transmitted): yes (1)/no (0)
    Xfree_t = model.addVars(whitebox_count, vtype=GRB.BINARY)

    """CONSTRAINTS"""
    # minimize makespan
    model.setObjective(makespan, GRB.MINIMIZE)
    model.Params.timeLimit = 180.0

    # end time is when the last filled whitebox finished processing all its images
    model.addConstrs((makespan >= (PV_t[t] + wb_t[t][0]) * (1-Xfree_t[t])
                      for t in range(whitebox_count)))

    # sets Xfree_t to be (at least) 1 if there is no image in the whitebox
    model.addConstrs((gp.quicksum(X_i_t[i, t] for i in range(len(images)))
                      # Xfree_t is binary, no need for another <= 1 constraint on it
                      + Xfree_t[t]
                      >= 1
                      for t in range(whitebox_count)))

    # Xfree_t should be 0 if there is at least one image being transmitted in whitebox t
    model.addConstrs((gp.quicksum(X_i_t[i, t] for i in range(len(images)))
                      * Xfree_t[t]
                      == 0
                      for t in range(whitebox_count)))

    # transmit each image exactly once
    model.addConstrs((gp.quicksum(X_i_t[i, t]
                                  for t in range(whitebox_count)) == 1
                      for i in range(len(images))))

    # total transmitting time in a whitebox is the sum of image lengths of images assigned to that whitebox...
    model.addConstrs((PV_t[t] == gp.quicksum(f_i[i] * X_i_t[i, t]
                                             for i in range(len(images)))
                      for t in range(whitebox_count)))

    # ... but is upperbounded by the length of the whitebox
    model.addConstrs((PV_t[t] <= wb_t[t][1]
                      for t in range(whitebox_count)))

    # SOLVE, RECONSTRUCT AND PRINT(optional) THE RESULTS
    model.optimize()

    # dummy start times, to catch errors
    end_time = -1
    image_starts = [-1]
    if model.status == GRB.OPTIMAL:
        _ = image_starts.pop()  # remove the dummy start time
        end_time = model.ObjVal

        print("\nWhite box decision variables:")
        for (i, t) in X_i_t:
            if X_i_t[i, t].X > 0:  # image i was assigned to whitebox t
                print(f"Image {i} assigned to whitebox {t}")
        print("All other X_i_t are 0")

        print('Whitebox contains:')
        for t in PV_t:
            print(
                f"wb{t} is {Xfree_t[t].X} empty with volume {PV_t[t].X} of images")

        timers = [0]*whitebox_count
        # initialize by setting to start of whitebox
        for t in range(whitebox_count):
            timers[t] = wb_t[t][0]

        # add the image lengths
        for j in range(len(images)):
            for t in range(whitebox_count):
                if X_i_t[j, t].X == 1:
                    image_starts.append(timers[t])
                    timers[t] += f_i[j]

        print(f"\nImage starting times: {image_starts}")
        print(f"=====> END TIME: {end_time} <=====")

    elif model.status == GRB.INFEASIBLE:
        print('\nNo solution: check input or constraints! Reason (gurobipy):')
        iis = model.computeIIS()  # Irreducible Infeasible Subsystem, https://support.gurobi.com/hc/en-us/articles/360029969391-How-do-I-determine-why-my-model-is-infeasible-
        print(iis)

    return (end_time, image_starts)

def calculate_whiteboxes(blackouts):
    """This function calculates the spaces between the blackouts.
    Returns a list of tuples with all the whiteboxes

    Args:
        blackouts (List[Tuple[int, int]]): A list with all the blackouts given as tuples with the start and end-time of the blackouts

    Returns:
        List[Tuple[int, int]]: Returns all the (start, duration) times of the whiteboxes (spaces between blackouts)
    """    

    if not blackouts:  # no black boxes, so whitebox length is infinite
        return [(0, GRB.INFINITY)]


    # Add the first whitebox that goes from t=0 until the begin of the first blackout
    whiteboxes = list()
    whiteboxes= [(0, blackouts[0][0])]

    # for every blackout add a whitebox from the end of the previous blackout until the start of blackout i
    for i in range(1, len(blackouts)):
        start_of_whitebox = blackouts[i-1][1]
        end_of_whitebox = blackouts[i][0]
        whiteboxes.append((start_of_whitebox, end_of_whitebox - start_of_whitebox))

    # add a whitebox from the end time of the last blackout to infinity
    whiteboxes.append((blackouts[-1][1], GRB.INFINITY))

    return whiteboxes
